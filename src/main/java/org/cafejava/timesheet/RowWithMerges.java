package org.cafejava.timesheet;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.List;
import java.util.Objects;

public class RowWithMerges {
    private final Row row;
    private final List<CellRangeAddress> merges;

    /**
     * Конструктор
     *
     * @param row строка
     */
    public RowWithMerges(Row row) {
        this.row = Objects.requireNonNull(row);
        merges = POIHelper.getMergesForRow(row);
    }

    public Row getRow() {
        return row;
    }

    public List<CellRangeAddress> getMerges() {
        return merges;
    }
}
