package org.cafejava.timesheet;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;

import static java.lang.Math.max;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.apache.poi.ss.util.CellUtil.getCell;
import static org.apache.poi.ss.util.CellUtil.getRow;

public final class POIHelper {
    private static final Locale RUSSIA = new Locale("ru");

    private POIHelper() {
    }

    /**
     * Скопировать строку
     *
     * @param rowFrom  исходная строка
     * @param rowTo    строка назначения
     * @param merges   объединенные строки
     * @param styleMap кеш стилей
     */
    public static void copyRow(Row rowFrom, Row rowTo, List<CellRangeAddress> merges, Map<CellStyle, CellStyle> styleMap) {
        rowTo.setRowStyle(rowFrom.getRowStyle());
        rowTo.setHeight(rowFrom.getHeight());
        for (Cell cellFrom : rowFrom) {
            final Cell cellTo = rowTo.createCell(cellFrom.getColumnIndex(), cellFrom.getCellType());
            copyCell(cellFrom, cellTo, styleMap);
        }

        for (CellRangeAddress merge : merges) {
            rowTo.getSheet().addMergedRegion(copyCellRangeAddress(merge, rowTo.getRowNum(), merge.getFirstColumn()));
        }
    }

    /**
     * Скопировать строку сохраняя все мержи
     *
     * @param rowFrom  исходная строка
     * @param rowTo    строка назначения
     * @param merges   объединенные строки
     * @param styleMap кеш стилей
     */
    public static void copyRowIdentically(Row rowFrom, Row rowTo, List<CellRangeAddress> merges, Map<CellStyle, CellStyle> styleMap) {
        rowTo.setRowStyle(rowFrom.getRowStyle());
        rowTo.setHeight(rowFrom.getHeight());
        for (Cell cellFrom : rowFrom) {
            final Cell cellTo = rowTo.createCell(cellFrom.getColumnIndex(), cellFrom.getCellType());
            copyCell(cellFrom, cellTo, styleMap);
        }

        for (CellRangeAddress merge : merges) {
            rowTo.getSheet().addMergedRegion(copyCellRangeAddress(merge, rowTo.getRowNum(), merge.getFirstColumn()));
        }
    }

    /**
     * Удалить строки из страницы
     *
     * @param sheet     страница
     * @param fromIndex индекс строки с котрой начать удаление
     * @param count     число строк
     */
    public static void removeRows(Sheet sheet, int fromIndex, int count) {
        // Размердживаем ячейки
        final Set<Integer> rangesToRemove = new TreeSet<>(Comparator.reverseOrder());
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            final CellRangeAddress cellRange = sheet.getMergedRegion(i);
            if (cellRange.getFirstRow() < fromIndex + count && fromIndex <= cellRange.getLastRow()) {
                rangesToRemove.add(i);
            }
        }
        rangesToRemove.forEach(sheet::removeMergedRegion);
        for (int i = fromIndex; i < fromIndex + count; i++) {
            sheet.removeRow(getRow(i, sheet));
        }
        shiftRows(sheet, fromIndex + count, sheet.getLastRowNum(), -count);
    }

    /**
     * Сдвинуть строки
     *
     * @param sheet     Страница
     * @param fromIndex строка начала сдвига
     * @param toIndex   строка окончания сдвига
     * @param n         число строк для сдвига
     */
    public static void shiftRows(Sheet sheet, int fromIndex, int toIndex, int n) {
        final List<Short> heights = new ArrayList<>(toIndex + 1 - fromIndex);
        for (int i = fromIndex; i <= toIndex; i++) {
            heights.add(getRow(i, sheet).getHeight());
        }
        sheet.shiftRows(fromIndex, toIndex, n);
        int index = 0;
        for (int i = fromIndex + n; i <= toIndex + n; i++) {
            getRow(i, sheet).setHeight(heights.get(index++));
        }
    }

    /**
     * Получить объединенные ячейки
     *
     * @param row строка
     * @return ячейки
     */
    public static List<CellRangeAddress> getMergesForRow(Row row) {
        final List<CellRangeAddress> merges = new ArrayList<>();
        for (int i = 0; i < row.getSheet().getNumMergedRegions(); i++) {
            final CellRangeAddress merge = row.getSheet().getMergedRegion(i);
            if (merge.getFirstRow() == row.getRowNum()) {
                merges.add(merge);
            }
        }
        return merges;
    }

    /**
     * Копирование стилей
     *
     * @param template откуда
     * @param target   куда
     */
    public static void copySheetStyles(Sheet template, Sheet target) {
        // Копируем ширины колонок
        int maxColumnIndex = 0;
        for (Row templateRow : template) {
            maxColumnIndex = max(maxColumnIndex, templateRow.getLastCellNum());
        }
        for (int i = 0; i <= maxColumnIndex; i++) {
            target.setColumnWidth(i, template.getColumnWidth(i));
        }

        // Копируем хеадер и футер
        target.getHeader().setLeft(template.getHeader().getLeft());
        target.getHeader().setCenter(template.getHeader().getCenter());
        target.getHeader().setRight(template.getHeader().getRight());
        target.getFooter().setLeft(template.getFooter().getLeft());
        target.getFooter().setCenter(template.getFooter().getCenter());
        target.getFooter().setRight(template.getFooter().getRight());

        // Копируем ориентацию, отступы и размер бумаги
        final PrintSetup targetPS = target.getPrintSetup();
        final PrintSetup templatePS = template.getPrintSetup();

        targetPS.setLandscape(templatePS.getLandscape());
        targetPS.setPaperSize(templatePS.getPaperSize());
        targetPS.setHeaderMargin(templatePS.getHeaderMargin());
        targetPS.setFooterMargin(templatePS.getFooterMargin());
        targetPS.setScale(templatePS.getScale());
        target.setMargin(Sheet.LeftMargin, template.getMargin(Sheet.LeftMargin));
        target.setMargin(Sheet.RightMargin, template.getMargin(Sheet.RightMargin));
        target.setMargin(Sheet.TopMargin, template.getMargin(Sheet.TopMargin));
        target.setMargin(Sheet.BottomMargin, template.getMargin(Sheet.BottomMargin));

        target.setDisplayGridlines(template.isDisplayGridlines());
        target.setPrintGridlines(template.isPrintGridlines());
    }


    /**
     * Установить значение в ячейку
     *
     * @param row     строка
     * @param cellNum номер ячейки
     * @param value   значение
     */
    public static void setCellValue(Row row, int cellNum, Integer value) {
        if (value != null) {
            getCell(row, cellNum).setCellValue(value.doubleValue());
        } else {
            getCell(row, cellNum).setCellValue((String) null);
        }
    }

    /**
     * Установить значение в ячейку
     *
     * @param row          строка
     * @param cellNum      номер ячейки
     * @param value        значение
     * @param defaultValue значение, если value == null
     */
    public static void setCellValue(Row row, int cellNum, Integer value, String defaultValue) {
        if (value != null) {
            getCell(row, cellNum).setCellValue(value.doubleValue());
        } else {
            getCell(row, cellNum).setCellValue(defaultValue);
        }
    }

    /**
     * Установить значение в ячейку
     *
     * @param row     строка
     * @param cellNum номер ячейки
     * @param value   значение
     */
    public static void setCellValue(Row row, int cellNum, Long value) {
        if (value != null) {
            getCell(row, cellNum).setCellValue(value.doubleValue());
        } else {
            getCell(row, cellNum).setCellValue((String) null);
        }
    }

    /**
     * Установить значение в ячейку
     *
     * @param row          строка
     * @param cellNum      номер ячейки
     * @param value        значение
     * @param defaultValue значение, если value == null
     */
    public static void setCellValue(Row row, int cellNum, Long value, String defaultValue) {
        if (value != null) {
            getCell(row, cellNum).setCellValue(value.doubleValue());
        } else {
            getCell(row, cellNum).setCellValue(defaultValue);
        }
    }

    /**
     * Установить значение в ячейку
     *
     * @param row     строка
     * @param cellNum номер ячейки
     * @param value   значение
     */
    public static void setCellValue(Row row, int cellNum, Boolean value) {
        getCell(row, cellNum).setCellValue(Boolean.TRUE.equals(value) ? "Да" : "Нет");
    }

    /**
     * Установить значение в ячейку
     *
     * @param row     строка
     * @param cellNum номер ячейки
     * @param value   значение
     */
    public static void setCellValue(Row row, int cellNum, String value) {
        getCell(row, cellNum).setCellValue(value);
    }

    /**
     * Установить значение в ячейку
     *
     * @param row          строка
     * @param cellNum      номер ячейки
     * @param value        значение
     * @param defaultValue значение, если value == null
     */
    public static void setCellValue(Row row, int cellNum, String value, String defaultValue) {
        if (value != null) {
            getCell(row, cellNum).setCellValue(value);
        } else {
            getCell(row, cellNum).setCellValue(defaultValue);
        }
    }

    /**
     * Установить значение в ячейку
     *
     * @param row     строка
     * @param cellNum номер ячейки
     * @param value   значение
     */
    public static void setCellValue(Row row, int cellNum, BigDecimal value) {
        if (value != null) {
            getCell(row, cellNum).setCellValue(value.doubleValue());
        } else {
            getCell(row, cellNum).setCellValue((String) null);
        }
    }

    /**
     * Установить значение в ячейку
     *
     * @param row          строка
     * @param cellNum      номер ячейки
     * @param value        значение
     * @param defaultValue значение, если value == null
     */
    public static void setCellValue(Row row, int cellNum, BigDecimal value, String defaultValue) {
        if (value != null) {
            getCell(row, cellNum).setCellValue(value.doubleValue());
        } else {
            getCell(row, cellNum).setCellValue(defaultValue);
        }
    }

    /**
     * Установить значение в ячейку
     *
     * @param row      строка
     * @param index    индекс  ячейки
     * @param o        объект
     * @param toString функция преобразования в строку для данного объекта
     * @param <T>      класс
     */
    public static <T> void setCellValue(Row row, int index, T o, Function<T, String> toString) {
        getCell(row, index).setCellValue(o != null ? toString.apply(o) : null);
    }


    /**
     * Копирование объединенных ячеек
     *
     * @param from      Страница с исходными данными
     * @param mergeFrom объекдинеие ячеек для копирования
     * @param rowTo     строка назначение (строка куда копируем)
     * @param columnTo  номер колонки куда копируем
     * @param styleMap  кеш стилей
     */
    public static void copyMergedCell(Sheet from, CellRangeAddress mergeFrom, Row rowTo, int columnTo, Map<CellStyle, CellStyle> styleMap) {

        //From section
        int firstRow = mergeFrom.getFirstRow();
        int rowQty = mergeFrom.getLastRow() - firstRow + 1;
        int firstColumn = mergeFrom.getFirstColumn();
        int cellQty = mergeFrom.getLastColumn() - firstColumn + 1;

        //To section
        Sheet to = rowTo.getSheet();
        int startRow = rowTo.getRowNum();

        //Копируем ячейки
        for (int i = 0; i < rowQty; i++) {
            Row rowFrom = from.getRow(firstRow + i);
            Row destination = to.getRow(startRow + i);
            for (int j = 0; j < cellQty; j++) {
                Cell cellFrom = rowFrom.getCell(firstColumn + j);
                Cell cellTo = destination.getCell(columnTo + j);
                if (cellTo == null) {
                    cellTo = destination.createCell(columnTo + j);
                }
                copyCell(cellFrom, cellTo, styleMap);
            }
        }
        //Делаем аналогичное объединение ячеек
        to.addMergedRegion(new CellRangeAddress(startRow, startRow + rowQty - 1, columnTo, columnTo + cellQty - 1));
    }

    /**
     * Копировать ячейку (стили и данные) используя кеш стилей
     *
     * @param from     исходная ячейка
     * @param to       куда копировать
     * @param styleMap кеш стилей
     */
    public static void copyCell(Cell from, Cell to, Map<CellStyle, CellStyle> styleMap) {
        CellStyle style;

        if (styleMap.containsKey(from.getCellStyle())) {
            style = styleMap.get(from.getCellStyle());
        } else {
            style = to.getSheet().getWorkbook().createCellStyle();
            style.cloneStyleFrom(from.getCellStyle());
            styleMap.put(from.getCellStyle(), style);
        }
        to.setCellStyle(style);

        copyCellValue(from, to);
    }

    private static void copyCellValue(Cell from, Cell to) {

        switch (from.getCellType()) {
            case BLANK:
                to.setCellValue(from.getStringCellValue());
                break;
            case BOOLEAN:
                to.setCellValue(from.getBooleanCellValue());
                break;
            case ERROR:
                to.setCellErrorValue(from.getErrorCellValue());
                break;
            case FORMULA:
                to.setCellFormula(from.getCellFormula());
                break;
            case NUMERIC:
                to.setCellValue(from.getNumericCellValue());
                break;
            case STRING:
                to.setCellValue(from.getRichStringCellValue());
                break;
            default:
                //NOTHING. USING FOR CHECKSTYLE
                break;
        }
    }


    private static CellRangeAddress copyCellRangeAddress(CellRangeAddress address, int toRow, int toColumn) {
        int deltaRows = address.getLastRow() - address.getFirstRow();
        int deltaColumns = address.getLastColumn() - address.getFirstColumn();
        return new CellRangeAddress(toRow, toRow + deltaRows, toColumn, toColumn + deltaColumns);
    }

    /**
     * Заменить плейсходеры на текст
     *
     * @param workbook документ
     * @param params   плейсхолдеры
     */
    public static void resolvePlaceholders(Workbook workbook, Map<String, String> params) {
        int sheetsCount = requireNonNull(workbook).getNumberOfSheets();
        if (requireNonNull(params).isEmpty()) {
            return;
        }
        for (int i = 0; i < sheetsCount; i++) {
            Sheet sheet = requireNonNull(workbook.getSheetAt(i), "No sheet at" + i);
            resolvePlaceholders(sheet, params);
        }
    }

    /**
     * Заменить плейсходеры на текст
     *
     * @param sheet  страница документа
     * @param params плейсхолдеры
     */
    public static void resolvePlaceholders(Sheet sheet, Map<String, String> params) {
        requireNonNull(sheet).getLastRowNum();
        if (requireNonNull(params).isEmpty()) {
            return;
        }
        for (int i = max(0, sheet.getFirstRowNum()); i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            if (isNull(row)) {
                continue;
            }
            resolvePlaceholders(row, params);
        }
    }

    /**
     * ,
     * Заменить плейсходеры на текст
     *
     * @param row    строка страницы документа
     * @param params плейсхолдеры
     */
    public static void resolvePlaceholders(Row row, Map<String, String> params) {
        requireNonNull(row);
        if (requireNonNull(params).isEmpty()) {
            return;
        }
        for (int i = max(0, row.getFirstCellNum()); i <= row.getLastCellNum(); i++) {
            Cell cell = row.getCell(i);
            if (isNull(cell)) {
                continue;
            }
            resolvePlaceholders(cell, params);
        }
    }

    /**
     * Заменить плейсходеры на текст
     *
     * @param cell   ячейка документа
     * @param params плейсхолдеры
     */
    public static void resolvePlaceholders(Cell cell, Map<String, String> params) {
        requireNonNull(cell);
        if (requireNonNull(params).isEmpty()) {
            return;
        }
        if (!CellType.STRING.equals(cell.getCellTypeEnum())) {
            return;
        }

        params.keySet().stream()
                .filter(placeholder -> cell.getStringCellValue().contains(placeholder))
                .forEach(placeholder -> {
                    String text = cell.getStringCellValue();
                    cell.setCellValue(text.replaceAll(placeholder, params.get(placeholder)));
                });
    }
}
