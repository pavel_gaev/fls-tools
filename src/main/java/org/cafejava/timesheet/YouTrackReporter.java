package org.cafejava.timesheet;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.nio.file.Path;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

public class YouTrackReporter {


    public static void main(String... args) throws IOException {
        requireNonNull(args, "Не указан путь к файлу с данными");
        if (args.length == 0) {
            throw new IllegalArgumentException("Не указан путь к файлу с данными");
        }
        Path dataFilePath = Path.of(requireNonNull(args[0]));
        File file = requireNonNull(dataFilePath.toFile());
        if (isNull(file) || !file.exists()) {
            throw new IllegalArgumentException("Неверный путь к файлу с данными");
        }
        List<UserTime> userTimes;
        try (InputStream inputStream = new FileInputStream(file)) {
            userTimes = getUserTime2(inputStream);
        }
        Path reportFilePath = getOutputFilePath(args);
        try (OutputStream outputStream = new FileOutputStream(reportFilePath.toFile())
        ) {
            writeReport(userTimes, outputStream);
        }
    }

    private static Path getOutputFilePath(String[] args) {
        if (args.length == 1) {
            Path dataFilePath = Path.of(requireNonNull(args[0]));
            String filename = dataFilePath.getFileName().toString();
            int idx = filename.lastIndexOf(".");
            String reportFileName = (idx == -1 ? filename : filename.substring(0, idx)).concat(".xlsx");
            System.out.println(reportFileName);
            return dataFilePath.getParent().resolve(reportFileName);
        }
        return Path.of(requireNonNull(args[1]));
    }

    @Deprecated
    public static List<UserTime> getUserTime(InputStream csvFile) throws IOException {
        requireNonNull(csvFile);
        CSVParser parser = CSVParser.parse(csvFile, UTF_8, CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .withHeader("description", "date", "duration", "estimate", "authorLogin", "authorName", "issueId", "issueSummary", "groupName", "type"));
        List<UserTime> timeList = new ArrayList<>();
        for (CSVRecord csvRecord : parser) {
            timeList.add(new UserTime()
                    .setAuthorName(csvRecord.get(5))
                    .setDate(Instant.ofEpochMilli(Long.valueOf(csvRecord.get(1))))
                    .setDuration(Float.valueOf(csvRecord.get(2)) / 60f)
                    .setIssueId(csvRecord.get(6))
                    .setIssueSummary(csvRecord.get(7))
            );
        }
        Comparator<UserTime> comparator = Comparator.comparing(UserTime::getDate)
                .thenComparing(UserTime::getAuthorName)
                .thenComparing(UserTime::getIssueId);

        timeList.sort(comparator);
        return timeList;
    }

    public static List<UserTime> getUserTime2(InputStream csvFile) throws IOException {
        requireNonNull(csvFile);
        CSVParser parser = CSVParser.parse(csvFile, UTF_8, CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .withHeader("Group name", "Item", "Spent time"));

        List<UserTime> timeList = new ArrayList<>();
        for (CSVRecord csvRecord : parser) {
            String issueStr = csvRecord.get(1);
            if (isNull(issueStr) || issueStr.isEmpty()) {
                continue;
            }
            String group = csvRecord.get(0);
            int i = group.lastIndexOf(' ');
            if (i == -1) {
                throw new IllegalStateException("Could not get correct data in row:" + csvRecord);
            }
            String authorName = group.substring(0, i);
            Instant instant = Instant.parse(group.substring(i + 1) + "T00:00:00Z");
            int i2 = issueStr.indexOf(':');
            if (i2 == -1) {
                throw new IllegalStateException("Could not get correct data in row:" + csvRecord);
            }

            timeList.add(new UserTime()
                    .setAuthorName(authorName)
                    .setDate(instant)
                    .setDuration(Float.valueOf(csvRecord.get(2)) / 60f)
                    .setIssueId(issueStr.substring(0, i2))
                    .setIssueSummary(issueStr.substring(i2+1).trim())
            );
        }
        Comparator<UserTime> comparator = Comparator.comparing(UserTime::getDate)
                .thenComparing(UserTime::getAuthorName)
                .thenComparing(UserTime::getIssueId);

        timeList.sort(comparator);
        return timeList;
    }

    public static void writeReport(List<UserTime> userTimes, OutputStream outputStream) throws IOException {
        requireNonNull(userTimes);
        requireNonNull(outputStream);

        Map<CellStyle, CellStyle> styleMap = new HashMap<>();

        List<String> users = new ArrayList<>(userTimes.stream().map(UserTime::getAuthorName).collect(Collectors.toSet()));
        users.sort(String::compareTo);


        Workbook workbook = WorkbookFactory.create(YouTrackReporter.class.getResourceAsStream("/2019_12_team_report.xlsx"));
        requireNonNull(workbook);
        Sheet originalSheet = workbook.getSheetAt(0);
        Sheet sheet = workbook.createSheet();

        POIHelper.copySheetStyles(originalSheet, sheet);

        RowWithMerges userRow = new RowWithMerges(originalSheet.getRow(0));
        int currentRow = 0;
        for (String user : users) {
            Row row = sheet.createRow(currentRow++);
            POIHelper.copyRowIdentically(userRow.getRow(), row, userRow.getMerges(), styleMap);
            row.getCell(4).setCellValue(user);
        }

        //Всего
        RowWithMerges tmp = new RowWithMerges(originalSheet.getRow(1));
        Row row = sheet.createRow(currentRow++);
        POIHelper.copyRowIdentically(tmp.getRow(), row, tmp.getMerges(), styleMap);

        //Формула для всего =))
        tmp = new RowWithMerges(originalSheet.getRow(2));
        row = sheet.createRow(currentRow++);
        POIHelper.copyRowIdentically(tmp.getRow(), row, tmp.getMerges(), styleMap);
        row.getCell(5).setCellFormula(String.format("SUM(F1:F%d)", users.size()));

        //Пустая строка
        sheet.createRow(currentRow++);
        //Заголовки к таблице
        tmp = new RowWithMerges(originalSheet.getRow(4));
        row = sheet.createRow(currentRow++);
        POIHelper.copyRowIdentically(tmp.getRow(), row, tmp.getMerges(), styleMap);
        //Данные в таблицу
        final int startRow = currentRow + 1;
        tmp = new RowWithMerges(originalSheet.getRow(5));
        for (UserTime userTime : userTimes) {
            row = sheet.createRow(currentRow++);
            POIHelper.copyRowIdentically(tmp.getRow(), row, tmp.getMerges(), styleMap);
            row.getCell(1).setCellValue(userTime.getIssueId());
            row.getCell(2).setCellValue(userTime.getIssueSummary());
            row.getCell(3).setCellValue(new Date(userTime.getDate().toEpochMilli()));
            row.getCell(4).setCellValue(userTime.getAuthorName());
            row.getCell(5).setCellValue(userTime.getDuration());
        }

        //СУММЕСЛИ($E$6:$E$85; E1;$F$6:$F$85)
        final int endRow = currentRow;
        for (int i = 0; i < users.size(); i++) {
            sheet.getRow(i).getCell(5).setCellFormula(String.format("SUMIF($E$%d:$E$%d, E%d,$F$%d:$F$%d)", startRow, endRow, i + 1, startRow, endRow));
        }
        workbook.removeSheetAt(0);
        workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
        workbook.write(outputStream);
    }
}
