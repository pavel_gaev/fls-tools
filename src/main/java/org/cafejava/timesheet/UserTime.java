package org.cafejava.timesheet;

import java.time.Instant;

public class UserTime {
    //ФИО
    private String authorName;
    //Номер таски
    private String issueId;
    //Назвение таски
    private String issueSummary;
    //Дата
    private Instant date;
    //В часах
    private Float duration;


    public String getAuthorName() {
        return authorName;
    }

    public UserTime setAuthorName(String authorName) {
        this.authorName = authorName;
        return this;
    }

    public String getIssueId() {
        return issueId;
    }

    public UserTime setIssueId(String issueId) {
        this.issueId = issueId;
        return this;
    }

    public String getIssueSummary() {
        return issueSummary;
    }

    public UserTime setIssueSummary(String issueSummary) {
        this.issueSummary = issueSummary;
        return this;
    }

    public Instant getDate() {
        return date;
    }

    public UserTime setDate(Instant date) {
        this.date = date;
        return this;
    }

    public Float getDuration() {
        return duration;
    }

    public UserTime setDuration(Float duration) {
        this.duration = duration;
        return this;
    }

    @Override
    public String toString() {
        return "UserTime{" +
                "authorName='" + authorName + '\'' +
                ", issueId='" + issueId + '\'' +
                ", issueSummary='" + issueSummary + '\'' +
                ", date=" + date +
                ", duration=" + duration +
                '}';
    }
}
