package org.cafejava.timesheet;

import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class YouTrackReporterTest {

    @Test
    void getUserTime() throws IOException {
        try (
                InputStream is = this.getClass().getClassLoader().getResourceAsStream("2019_12_team_report.csv");
        ) {
            List<UserTime> timeList = YouTrackReporter.getUserTime(is);
            assertNotNull(timeList);
            System.out.println(timeList);
        }
    }

    @Test
    void getUserTime2() throws IOException {
        try (
                InputStream is = this.getClass().getClassLoader().getResourceAsStream("2020_01_team_report.csv");
        ) {
            List<UserTime> timeList = YouTrackReporter.getUserTime2(is);
            assertNotNull(timeList);
            System.out.println(timeList);
        }
    }

    @Test
    void writeReport() throws IOException {

        List<UserTime> timeList;
        try (
                InputStream is = this.getClass().getClassLoader().getResourceAsStream("2020_01_team_report.csv");
        ) {
            timeList = YouTrackReporter.getUserTime2(is);
            assertNotNull(timeList);
        }


        try (OutputStream outputStream = new FileOutputStream("/usr/1.xlsx")
        ) {
            YouTrackReporter.writeReport(timeList, outputStream);
        }
    }
}